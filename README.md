# Sophie Domain

Provides simple components to help with Domain objects. Collection, model, http readers, http writers, create, save, delete, update models and collection
It uses **Promises**.

**Part of the framework [Sophie](https://bitbucket.org/xtrimsystems/sophie)**

[![npm version](https://badge.fury.io/js/sophie-domain.svg)](https://badge.fury.io/js/sophie-domain)
[![npm dependencies](https://david-dm.org/xtrimsystems/sophie-domain.svg)](https://www.npmjs.com/package/sophie-domain?activeTab=dependencies)
[![npm downloads](https://img.shields.io/npm/dm/sophie-domain.svg)](https://www.npmjs.com/package/sophie-domain)

## INSTALLATION
```shell
yarn add sophie-domain
```

## USAGE

## Changelog
[Changelog](https://bitbucket.org/xtrimsystems/sophie-domain/src/master/CHANGELOG.md)

## Contributing [![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

## License
This software is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT). See [LICENSE](https://bitbucket.org/xtrimsystems/sophie-domain/src/master/LICENSE) for the full license.
