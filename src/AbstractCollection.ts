import { AbstractModel } from './AbstractModel';

export abstract class AbstractCollection
{
	protected models: AbstractModel[];

	protected constructor (models: AbstractModel[] = [])
	{
		this.models = models;
	}

	public push (model: AbstractModel): void
	{
		this.models = [model].concat(this.models);
	}

	public blow (models: AbstractModel[]): void
	{
		this.models = this.models.concat(models);
	}

	public pull (model: AbstractModel): void
	{
		const index: number = this.models.indexOf(model);
		/* tslint:disable no-bitwise */
		if (!~index) {
			/* tslint:enable no-bitwise */
			throw new Error('Model not found in collection');
		}
		this.models.splice(index, 1);
	}

	public length (): number
	{
		return this.models.length;
	}

	public getModels (): AbstractModel[]
	{
		return this.models;
	}
}
