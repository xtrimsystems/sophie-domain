/* tslint:disable no-any */
import { JsonRequestContentType, Query, XhrResponse } from 'sophie-http';

import { AbstractModel } from '../AbstractModel';
import { Reader } from '../Interfaces';
import { ModelJsonParser } from '../Parsers';

export class ModelJsonReader extends Query implements Reader
{
	private readonly requestContentType: JsonRequestContentType;
	private readonly parser: ModelJsonParser;

	public constructor (
		options?: { parser?: ModelJsonParser },
	) {
		super();
		this.requestContentType = new JsonRequestContentType();
		if (options) {
			if (options.parser) {
				this.parser = options.parser;
			}
		}
	}

	public async readData (): Promise<AbstractModel[] | AbstractModel>
	{
		try {
			const xhrResponse: XhrResponse = await this.getData(this.requestContentType);

			return new Promise<AbstractModel[] | AbstractModel>((resolve, reject) => {
				try {
					const responseAsObject = JSON.parse(xhrResponse.text);
					if (responseAsObject.length === 0) {
						resolve([] as AbstractModel[]);
					}
					resolve(this.parse(responseAsObject));
				} catch (error) {
					reject(error);
				}
			});
		} catch (error) {
			throw new Error(error);
		}
	}

	private parse (data: any): AbstractModel[] | AbstractModel
	{
		if (data.length) {
			return this.parseCollection(data);
		} else {
			return this.parseModel(data);
		}
	}

	private parseModel (data: object): AbstractModel
	{
		if (this.parser) {
			try {
				return this.parser.parse(data);
			} catch (error) {
				throw new Error(error);
			}
		}

		return data as AbstractModel;
	}

	private parseCollection (data: any): AbstractModel[]
	{
		const models: AbstractModel[] = [];

		for (const dataModel of data)
		{
			if (this.parser) {
				try {
					models.push(this.parser.parse(dataModel));
				} catch (error) {
					throw new Error(error);
				}
			} else {
				models.push(dataModel);
			}
		}

		return models;
	}
}
