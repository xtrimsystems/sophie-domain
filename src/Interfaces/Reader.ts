/* tslint:disable no-any */
export interface Reader
{
	readData (...args: any[]): Promise<any>;
}
