/* tslint:disable no-any */
export interface Writer
{
	deleteModel (model: object): Promise<any>;

	saveModel (model: object): Promise<any>;

	updateModel (model: object): Promise<any>;
}
