/* tslint:disable no-any */
export interface Parser
{
	parse (...args: any[]): any;
}
