import { Command, JsonRequestContentType, XhrResponse } from 'sophie-http';

import { AbstractModel } from '../AbstractModel';
import { Writer } from '../Interfaces';
import { ModelJsonParser } from '../Parsers';

export class ModelJsonWriter extends Command implements Writer
{
	protected requestContentType: JsonRequestContentType;
	private readonly parser: ModelJsonParser;

	public constructor (
		options?: { parser?: ModelJsonParser },
	) {
		super();
		this.requestContentType = new JsonRequestContentType();
		if (options) {
			if (options.parser) {
				this.parser = options.parser;
			}
		}
	}

	public async deleteModel (model: object): Promise<AbstractModel>
	{
		try
		{
			const xhrResponse: XhrResponse = await this.deleteData(this.requestContentType, model);

			return this.returnModelAsPromise(xhrResponse);
		}
		catch (error)
		{
			throw new Error(error);
		}
	}

	public async saveModel (model: object): Promise<AbstractModel>
	{
		try
		{
			const xhrResponse: XhrResponse = await this.saveData(this.requestContentType, model);

			return this.returnModelAsPromise(xhrResponse);
		}
		catch (error)
		{
			throw new Error(error);
		}
	}

	public async updateModel (model: object): Promise<AbstractModel>
	{
		try
		{
			const xhrResponse: XhrResponse = await this.updateData(this.requestContentType, model);

			return this.returnModelAsPromise(xhrResponse);
		}
		catch (error)
		{
			throw new Error(error);
		}
	}

	private async returnModelAsPromise (xhrResponse: XhrResponse): Promise<AbstractModel>
	{
		return new Promise<AbstractModel>((resolve, reject) => {
			try {
				const responseAsObject = JSON.parse(xhrResponse.text);
				resolve(this.parseModel(responseAsObject));
			} catch (error) {
				reject(error);
			}
		});
	}

	private parseModel (data: object): AbstractModel
	{
		if (this.parser) {
			try {
				return this.parser.parse(data);
			} catch (error) {
				throw new Error(error);
			}
		} else {
			return data as AbstractModel;
		}
	}
}
