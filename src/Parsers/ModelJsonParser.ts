import { AbstractModel } from '../AbstractModel';
import { Parser } from '../Interfaces';

export abstract class ModelJsonParser implements Parser
{
	public abstract parse (json: object): AbstractModel;
}
