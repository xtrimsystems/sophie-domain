import { AbstractCollection, AbstractModel } from '../src';

describe('Test AbstractCollection', () => {
	describe('Can initialize with', () => {
		it('empty collection', () => {
			const abstractCollection = new TestCollection();
			expect(abstractCollection.getModels()).toEqual([]);
		});
		it('with a given collection', () => {
			const abstractModels = [];
			abstractModels.push(new TestModel());
			abstractModels.push(new TestModel());

			const abstractCollection = new TestCollection(abstractModels);
			expect(abstractCollection.getModels().length).toEqual(2);
		});
	});
});

class TestCollection extends AbstractCollection
{
	public constructor (models: AbstractModel[] = [])
	{
		super(models);
	}
}

class TestModel extends AbstractModel
{
}